
# This approach uses maven and the JDK to build the application inside the container.  Another approach
# would be to build an uberjar outside the container, and then just COPY the necessary files into the container.
# This would not require all the JARs to be downloaded, and could be done with a more lightweight base container
# (is this what the alpine docker images are good for?).

#
# Build stage
#
# FROM maven:3.6.0-jdk-11-slim AS build
FROM maven:3.8.4-jdk-11-slim AS build
COPY src     /home/app/src
COPY pom.xml /home/app
# RUN mvn -f /home/app/pom.xml clean package
WORKDIR /home/app

RUN mvn --no-transfer-progress  clean package assembly:single
# RUN mvn --no-transfer-progress  exec:java -Dexec.mainClass="com.andydemo.prototype.App"

RUN java -version
RUN mvn --version

# ENTRYPOINT + CMD = default container command arguments
# CMD can be overwritten.
# ENTRYPOINT cannot be overwritten.
# ENTRYPOINT mvn  -B --no-transfer-progress exec:java -Dexec.mainClass="com.andydemo.dataprof.ProfileApp"

# This works, but doesn't allow for customization
# ENTRYPOINT java -jar target/forpatad-0.0.2-SNAPSHOT-jar-with-dependencies.jar -sample_frequency 1 src/test/resources/test1.csv

ENTRYPOINT ["java", "-jar", "target/forpatad-0.0.2-SNAPSHOT-jar-with-dependencies.jar"]
CMD  [ "-sample_frequency", "1", "src/test/resources/test1.csv" ]

# See https://aws.amazon.com/blogs/opensource/demystifying-entrypoint-cmd-docker/
# cmd ls




# Build with 
#     docker build -f test.dockerfile  -t forpatad .

# Run with 
#      docker run forpatad
# For a data file test3.csv in the project root directory of host the following all work 
# MSYS_NO_PATHCONV=1 docker run -v $(pwd):/home/app/mydata forpatad -sample_frequency 1 /home/app/mydata/test3.csv
# MSYS_NO_PATHCONV=1 docker run -v $(pwd):/home/app/mydata forpatad -sample_frequency 1 mydata/test3.csv
# MSYS_NO_PATHCONV=1 docker run -v $(pwd):/mydata          forpatad -sample_frequency 1 /mydata/test3.csv
# (see https://tkit.dev/2020/03/06/mounting-a-docker-volume-from-git-bash/ )

# TODO How to export the log file???