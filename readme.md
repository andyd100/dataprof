


Document basic algorithms in readme file (for now)


# Introduction #

Document overview for both users and developers

FORPATAD is a tool for quickly summarizing, or profiling, text data files.

The goal is to make it super easy to get a good idea of the structure and content of data files
with minimal effort, simply by "pointing to" a data file.  This should not require additional effort
of already knowing how to parse the file, or opening some other tool in order to load the data.

The result is a
summary of the data in each column, indicating presumed data types, extreme values, column null density, etc.
Currently the program
only supports columnar data files (like CSV files). Some useful features include:
  + Detect column delimiter
  * Process compressed file archives in a variety of common formats including
  ** ZIP
  ** GZIP
  ** TAR
  * Process all the files in a specified directory
  * Process all the files in an archive file.



Here's how it works:

    Process the command line arguments
    Read the configuration
    For each file
        Read the file row by row, sampling rows to select and populating column data structures
           (sampleFileIntoColumns
        For each column
            analyze the column
        For each column
            report the column summary

      
     ProfileEngine.processDataFile()


 * TODO Document how to run the program on files, folders, and ZIPs

 * TODO High level file structure
 ** Whether or not there is a header column.  
 ** Document how column separator is inferred
 ** Document how column headers are inferred
 ** Document how field quote character is inferred
 
 * TODO How regexes are configured.
 * TODO How to extend with custom configuration files.


# Sample Data Files #

Downloading test data sets:
I have used geonames data files for testing:  http://download.geonames.org/export/


    wget http://download.geonames.org/export/dump/allCountries.zip
    unzip allCountries.zip

    wget http://download.geonames.org/export/dump/cities1000.zip
    unzip cities1000.zip

    wget wget https://www1.ncdc.noaa.gov/pub/data/swdi/stormevents/csvfiles/StormEvents_details-ftp_v1.0_d1950_c20170120.csv.gz

Build some zip files for testing those features.

    cd src/test/resources
    cp test1.csv test2.csv
    jar cvMf multiple_test.zip test1.csv test2.csv
    jar cvMf single_test.zip   test1.csv
    tar cvf  multiple_test.tar test1.csv test2.csv
    tar cvzf multiple_test.tgz test1.csv test2.csv


# EXAMPLES #

Some example invocations:

    mvn clean compile

    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp
    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-h"
    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args=""
    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 1 src/test/resources/test1.csv"
    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 1 ./src/test/resources/test1.csv"
    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 1 cities1000.txt"
    mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 513  allCountries.txt"

     mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 1 StormEvents_details-ftp_v1.0_d1950_c20170120.csv.gz

    FAILS mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 1 -quote_character DQUOTE tgz:///C:/Users/Andy/projects/data_profiler/forpatad/src/test/resources/multiple_test.tgz"

# Process the files in a directory #
mvn exec:java -Dexec.mainClass=com.andydean.dataprof.ProfileApp -Dexec.args="-sample_frequency 1 -quote_character DQUOTE src/test/resources"


# Distributing #

Instructions for building uberjar:

    mvn clean package assembly:single
    java -jar target/forpatad-0.0.2-SNAPSHOT-jar-with-dependencies.jar -sample_frequency 1 src/test/resources/test1.csv


    runit.bat -sample_frequency 1 src/test/resources/test1.csv


    mvn versions:display-dependency-updates


# Docker #

Build an image

    docker build -f test.dockerfile  -t forpatad .


Run the image

    docker run forpatad

    
TODO Include examples where other data     