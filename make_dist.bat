
call mvn clean package assembly:single

copy /Y  target\forpatad-0.0.2-SNAPSHOT-jar-with-dependencies.jar dd_dist
copy /Y  src\test\resources\test1.csv     dd_dist
copy /Y  readme.txt                       dd_dist
copy /Y  canadian_provinces.txt           dd_dist
copy /Y  usa_state_abbrev.txt             dd_dist

jar cvMf dd_dist.zip dd_dist

