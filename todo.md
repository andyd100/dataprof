
Goal:  Minimal configuration should be required of user

Note there is a difference between inferring column purpose and data validation.  Data validation is stricter - we don't necessarily want to reject
a column being of a particular type if there are relatively few exceptions.

[TOC]


# IN PROGRESS #
 - Enable running in docker.  There are (at least) two different ways to approach this.  Can do it just like the formulas project,
   where the JAR is built in docker so maven is required but that bloats the image, or can build the uberjar separate from docker
   and in that case the docker image only needs the JVM (not even JDK) and the uberjar.
 - Update README with clear instructions for how to get started
      - mvn build ... from source, and then run the class
      - mvn to build uberjar than use as a command line tool directly, or with a shell script wrapper.
      - download the uberjar (don't have a way to do this yet, TODO)
      - embedded in a JVM script.  Use groovy as example.
      - docker instance.  Include both full JDK and recompiling version, as well a a thinner version that just has JVM and uberjar assembly.   
 - The unit tests that reference propublica data no longer work since big datasets were removed from the repo.  Need to make changes, assure that cases are covered with a small data set.
 - File summary should include number of lines read, number of columns, and overall total number of non-null values (file density)
 - DONE Columns that are empty should be given a unique type.  Currently they are shown as text/classification.
 - DONE Make the column widths of the summary report configurable in the dataprof.properties file.
 - DONE Compute correlation between pairs of numeric columns
 - Show N first rows from the file as part of the result report.
 - Create test datasets of multiple files.
 - OneLiner column summary should expose all it's logic as an API.
 - Tune deciding something is a classification to a rule like if the number of unique values is less than 10% of the total num of values, it's probably a classification.
 - Rethink detecting classifications - like REGION_ID in 02_location
 - Cleanup readme.txt so it is truly an introduction.  Move some of the various invocations elsewhere (but keep them available).
 - Extend the unit tests.  Drive coverage up.
 - Create a script building a distribution that can be used.  May require readme file, as well as uberjar and us/canadian states files.
 - Include shell/batch wrapper for invoking the jar as well.  Use assembly plugin.
 - Implement --help/-h support, with full description of functionality.

 - Generate output as https://specs.frictionlessdata.io//table-schema/ or some other similar standard.  See also
   https://frictionlessdata.io/standards
 - Compute checksum of file so database of previous runs can be created, and changes detected.

# BUGS #
 - get an error when trying to read a file containing spaces in the name, like "Civil 1970 to 1987.txt"
 - get an error when using wildcard to match multiple files (on Windows, bash shell, at least) such as test*.csv
 - src/test/resources/marta/google_transit/shapes.txt reports that Max value of shape_pt_lon column is blank. See below.  Check
   if that is an actual value in the file. 
    Col# Column                    PK? type                         Width FW? Unique Density Min             Max
        ------                    --- ----                         ----- --- ------ ------- ---              ---
     1: shape_id                  -   number/unsigned integer          6 F      368   1.000 56371            103678
     2: shape_pt_lat              -   number/unsigned float            9 F    43226   1.000 33.43226         34.106599
     3: shape_pt_lon              -   number/signed float             10 F    39342   1.000 -84.670849
     4: shape_pt_sequence         -   number/unsigned integer          5 F    26668   1.000 1                26668
 


# CODE / DEVELOPMENT / DOCUMENTATION #
 - Make consistent project name, config file, package name, main class name.  
 - Tighten up exception handling.  I've allowed many methods to throw Exceptions, but they should handle them locally, at least with an error message.  

 - Change the DIRECTORY handling to use the Apache VFS stuff, instead of handling directories specially.
 - Put a facade in front of the file references so that a user doesn't have to know
       the specific syntax of VFS for local files, including both the protocol prefix (gz:, zip:, etc) and the full URL path.  Should be able to simply
       specify a relative path.
 - Properly handle paths regardless of using forward or backward slashes 


 - Extend the set of small unit test files that can be committed to bitbucket.  Design them for specific scenarios.  Support comments so the files can be self-explanatory.
 - Replace String.matches() use with a Matcher, to improve performance by precompiling regexes.  Test on allcountries.txt, since it takes long to run.
 
 - In configuration, distinguish between storage type (numeric, text, date) and more fine grained descriptions (names, emails, web URLs, locations, etc)
 - Create memory efficient variants for very large files.  One approach would be to process one line at a time.  Then only big memory consumers
   would be columns which are all unique values, which would all be stored in the set used to determine if a column is all unique values.  Another 
   option could be to pass over the file, storing and processing a single column at a time.  This might be relatively slow.
 - Create sample clients in groovy and jython
 - Copy all these notes to the google doc (maybe not - that may not be more convenient) 
   https://docs.google.com/document/d/1UJmOPKhbm7YcNO1j_iGImpg3VYkonB1TDEJmkE9aOSE/edit#heading=h.cb9e1k9z8poz
 - Include examples which work with some common downstream libraries.  For instance, work with OpenCsv, using 
   the data profiler to determine the column delimiter.
 - Make everything able to run with a reader, and be able to reset the position of the reader.
    Using reader will help to read from ZIP files as streams.

 - Raise test coverage percentages.  Fail build if below 80%.
 - Scan through TODOs in code - address them, leave them, or move them to this file as appropriate.
 - Use stderr for error messages as appropriate.
 - Add local Jenkins project. 
- Consider using gradle for build, perhaps in parallel with maven?  (DEFER, to keep the project reports which maven generates.

 - Loop through the set of column names to determine how wide to use as the max width for the column names.  Consider doing this for all the columns, to make output columns line up nicely.



# CONFIGURATION #

 - Allow config file to be passed in as argument (dataprof.properties)
 - Any secondary config should override/extend (not replace) the default configuration.  This allows secondary configs to only include changes.
 - Think up algorithm for determining quote character 
 - Enable separator character to be passed in (overriding what the file analysis determines)
 - Allow the command line arguments to also be read from properties file.  
 - For patterns in config files - typical is to indicate a regular expression
   but there should also be a way to indicate a full class name that can be used if people want to build their own matchers which are not regular expressions.
 - Need a way to categorize the patterns in the config files.   Built ins can distinguish between
   numeric, text, date but it may be good to allow end users to create their own categories as well.
   An example might be to indicate numeric codes such as postal codes be treated as text instead of numeric, (or both).
 - Bundle sets of standard files in resource directory, but make it easy for them to be ignored, replaced, or augmented.
 - Bundle industry specific matcher packages.  For example, for medical, scientific, business, or finance data sets (CUSIP, EAN, etc)
 - Consider extending specification of file with matching values to allow both fixed lists and regexes to be combined, ie anything from this file OR matching a regex.
 
 - get set up to run with performance profiler, to detect any obvious hotspots.
 - Allow log4j.properties to be specified on command line.  Currently log4j.properties is embedded in the resource directory.  
 
# REPORTING #

 - Do some analysis of the values within a row, for the entire file.  For example, if there are multiple date columns, could check if one is always greater 
   than the other, etc.
 - Show the number of rows in the file, and the number sampled, in the file summary.
 - Include ability to compare a new file with the profile of a previously saved file(set).   Basics would just be that column types and headers match, but then
   could also compare the distribution and ranges of columns (like does that new file cover a different range of dates).  
 - Return results as JSON, to prepare for running as a service that returns JSON results.
 - If no numeric values were encountered, don't display numeric summary info.
 - Output in "normal" and "verbose" levels.
 - Try to make a call on whether or not a field is a bunch of external keys.
 - Note if leading zeros are included in numeric field.
 - Summarize the input file - number of bytes, lines, columns, cells.  This may mean collecting file stats as well as column stats,
   or possibly aggregating the file stats from the column stats.
 - Use the fact that max length == min length to draw conclusions.  Indicate this in the column summary.

 - Maybe allow different ReportStrategy implementations, which can then implement the different output formats.
 - Output results as CSV file that can be easily imported into a spreadsheet.
 - Output results as HTML file, optionally - richer rendering for free.  Maybe make a thin groovy client to drive this, so HTML generation is easy.
 - An alternative to HTML output, or in addition, or combined - use a template engine to make generating nicely formatted output easier, and allow 
   customization of the layout.
 - Telling which patterns match is relatively easy, but automating decisions or at least conjecture about a particular
   column is more difficult to do.  Need to be able to define some order of precedence between the various patterns, perhaps
   segmented by type.  For example, how to distinguish between 5 digit ZIP codes and a plain integer field?  Column header can help.
   so can proximity to other location related columns.   So can the fact that
   a numeric field contains values all with the same number of digits, or with
   leading zeros.
 - Add commons-math.  Compute descriptive statistics on numeric columns: mean, stddev, coefficient of variance
 - Include date type columns when computing correlation between column pairs.
 - For date fields, convert the values to days and use those to report min and max. I think now it might be done as strings.

 - Can Bloom filter be used to efficiently tell if all entries in a column are unique?
 - Determine if there are duplicate rows.
 - For date type columns, show extreme dates, possibly also distribution of days of week and days of month (to
   see if all the dates are first of the month, last of the month, only weekdays, etc.
   date fields should have a variety of histograms - by day name, by day of month, first date, last date
 - For columns with lots of repeat values (especially if they are labels) show histogram of the most common values.  Show absolute counts and percentages.
 - Attempt to detect if there are tree/hierarchical structures among label text/classification values.
 
 - for numeric values, provide a way to check if they are within a range, or perhaps evaluate against an expression ( X > 10 && X < 20 ), say.
   support any JVM scripting language for this (use the formulas project) so arbitrary code can be inserted.  Javascript, Groovy, MVEL, etc
 - allow expression to be specified in the config file, or allow a script file to be named.  Script file will allow more elaborate algorithms.  
 
 - Show first 10 rows of output as a preview.  Make row dividers clear, maybe by formatting with padding so columns align, etc.

 - Summarize file inside ZIP archive without extracting to disk (streaming).
   
 - For multiple file sets such as directories and archives (ZIP, GZ, ...) list the set of files at the top of the output summary.
 
 - Category columns such as Canadian region in sales_pipes.csv don't have any type surviving
   in the report, under the Matched types: listing.  Perhaps there should be a "free text" default.
   
 - Create a fast utility that checks the structure of files (including in zips, etc).  Don't
   bother with summarizing data, but be thorough about determining separator and quotes, 
   column count, and possibly header rows.  This mode should have very low memory overhead.
   Part of output should be command line to run with column analysis (or maybe the difference
   is just a single flag, like --skipColumnAnalysis
 
 - Find out why result output sometimes repeats the header info.
 - Have a mode to save previous report values and summarize differences.
 - Locally hosted web app for viewing/exporting results.
 - Use previous files to create baseline for data quality reports on new files.
 - Persist results in a database, and use that as the source for reporting?  This would also make comparision of multiple data sets easier.
 - Output file parser for files, to read files, and separately write to different targets like SQL database, Spark, etc, etc.

# FILE DETECTION #
 - Need to detect if a file scheme is supported by VFS (use FileManager getScheme())
 - Need to distinguish between compressed archives that contain multiple files vs. a single file.
 - Detect file encodings (UTF-8, etc)
 - Need to be able to process files with no extension.  Either assume they are text (but try to verify) or allow file type to be passed in (or both).
 - Properly detect CSV type files with a different extension, such as .txt, .dlm, .dat (or no extension).  Currently for a .dat file the program says it is not recognizable, but renaming to .csv works.
 - Introduce something like findFiles() which takes command line argument
   and decides if it is a single file, list of files, or archive such as a
   folder or zip archive.
 - Consider integration with Apache Tika, https://tika.apache.org/.  I'm not certain this is helpful because currently Tika may only recognize CSV format files based on the name.  Something to look into.
   
   
# COLUMN DETECTION #

 - Have special designation for columns that are are dense and all have the same single value  (constant value)
 - Improve detection of date columns.  Common indicators are embedded slashes, dashes, or periods.  See https://stackoverflow.com/questions/11310065/how-to-detect-the-given-date-format-using-java
 - Detect time info included in date columns
 - Test running against web access logs.  Should handle such files and all their columns well, recognize the format as a web access log, etc.
   These are space separated files.  Example row is
          127.0.0.1 -  -  [18/Dec/2017:16:54:02 +0000] "POST http://localhost:55183/lb-web/admin HTTP/1.1" 200 109 "-" "-"  42

 - Handle files that include quoted fields.  Need to handle both if they quote every cell, and if they only do some.  Might try importing and counting the fields
   with NO quote character, if the number of columns is not consistent then try it using DOUBLE_QUOTE as the quote.
   
   example from Documenting Hate file, where the second line, second column is wrapped in double quotes and has embedded comma:
   2/13/17 15:37,House OKs bill to expand Kentucky's hate crimes law,FOX19,Cincinnati,OH,http://www.fox19.com/story/34494432/house-oks-bill-to-expand-kentuckys-hate-crimes-law,,
   2/13/17 16:56,"Amid Protests, 'Blue Lives Matter' Bill Passes Kentucky House",89.3 WFPL,Louisville,KY,http://wfpl.org/amid-protests-blue-lives-matter-bill-passes-state-house/,,
   2/13/17 18:37,Lafourche inmates charged with hate crimes in jail attack,Daily Comet,Thibodaux,LA,http://www.dailycomet.com/news/20170213/lafourche-inmates-charged-with-hate-crimes-in-jail-attack,,
   
 - Need a way to skip, and possibly remove and report on just the rows that don't confirm to the majority formatting.  Typically this will be based on non-matching
   column count because of quoted fields with embedded comma or other errors in the file.  Would be nice to be able to separate a big file into the "good" records and the "bad" records automatically.
   
 - Recognize numeric columns with all values between -1 and 1, or -100 and 100, as percentages.
 - Detect fields that themselves may be lists, such as a field that contains a bunch of commas or other delimiter characters (other than the file column delimiter)
 - Detect fields that contain HTML markup 
 - Detect fields that appear to contain long freeform text.
 - Detect if a column is already stored in sorted order (ascending or descending).
 - Detect if a field contains web addresses (starts with http, ftp, etc.
 - Detect if a fied contains email addresses.
 - Add support for recognizing any column which is ISO codes.  Start with country codes.  Allow user to extend.  Probably do this by
   allowing a file, instead of a regex, to be specified.  Contents of the value are the values to be compared with.
 - Support ISO currency codes, language codes, country codes.
 - Consider allowing a transform of the data to be compared.  For example, to check phone
   numbers could look to see if they are formatted as phone numbers like
     (404) 555-1234
   but another, possibly better, approach would be to remove the special chars and then
   see if there is exactly 9 digits left:
     4045551234
   since that would be more space-efficient to store in a database anyway.
 - Recognize common retail identifiers - UPC, EIN, etc.  See https://support.google.com/merchants/answer/160161?hl=en
     https://stackoverflow.com/questions/4270657/is-there-a-way-to-check-if-an-isbn-number-is-a-valid-number-before-storing-into
     https://commons.apache.org/proper/commons-validator/apidocs/org/apache/commons/validator/routines/checkdigit/package-summary.html
 - Recognize CUSIP, ISIN, SEDOL, values.
 - Recognize Ticker values.  
 - Create file column header recognizer (for starter, just assume first row is header if none of the values are all numeric or dates).
   can test with the data from geonames, which does not have column headers.
 - Need to figure out how to handle quoted fields.  This can be tricky.
 - Create file quote character recognizer 
 - Support for non-US numeric decimal and separators
 - Detect national languages. 
 - Detect if columns contain sentences or phrases - Capital letters, punctuation, relatively long strings.
 - Support having an archive of various recognizers, which can be updated and augmented separate from the code.  This could
   be used to allow special recognizers to be distributed.  Include option of compressing these for distribution.
 - ML approach would consider features for a column such as number of characters, containing decimal point, contains digits, contains non-digits, etc.
 
 - Use cosine similarity to compare columns from different data sources to see if they are similar sets of values.
 - Use Jaccard index to compare if columns have similar values.
 - See https://commons.apache.org/proper/commons-text/userguide.html for above


# INPUT FORMATS #
 - Support reading XLS and XLSX files.
    See http://stackoverflow.com/questions/2922692/to-get-columns-from-excel-files-using-apache-poi
 - Support reading OpenOffice/LibreOffice spreadsheets.
 - Support reading google spreadsheets
 - Consider support for reading from databases.  This may only be useful for providing consistent reporting, since data could be queried easily within 
   the database as well.  Plus there are already a variety of tools for running data profiling against a database.
    

# TESTING #

  Sources for sample data:
  
    - http://www.briandunning.com/sample-data/
    - http://www.mysqlperformanceblog.com/2011/02/01/sample-datasets-for-benchmarking-and-testing/
    - http://www.generatedata.com/
    - http://aws.amazon.com/publicdatasets/
    - https://projects.propublica.org/represent/expenditures
    - https://www.propublica.org/datastore/datasets
    - https://www.google.org/flutrends/about/
    - https://www.cdc.gov/nchs/data_access/vitalstatsonline.htm#Mortality_Multiple
    

# HYGIENE #
    mvn versions:display-dependency-updates
    
# Similar Projects #
    - http://jagg.sourceforge.net/
    - http://datacleaner.org/
    - http://metamodel.incubator.apache.org/
    - http://factual.com/products#data-mapping-and-cleaning
    - http://docs.aws.amazon.com/glue/latest/dg/what-is-glue.html
    - https://github.com/anovos/anovos and https://anovos.ai/
    

============================================
#  DEFER THESE #

Low Priority, but might be nice to have at some point.
    
 - Consider writing unit tests in groovy so multiline strings can be used for input easier.

  
============================================


# DONE #
 - DONE Upload project to hosted version control.
 - DONE Abbreviate OneLiner extreme range values with ... so that huge values don't end up in the report.
 - DONE log every N rows read, so progress is more visible.
 - DONE If every entry in a column is unique, and column is dense, suggest column is primary key.
 - DONE dates should be restricted to begin with 1 or 2 for YYYY.
 - DONE Guess any file as being text if it doesn't have an obvious extension (.dat, .dlm, .txt, ...)
 - DONE File with only one header, or zero rows, print junk for minimum and maximum value.  Looks like it is values that are all null.
 - DONE Show a column header for the column oneliner report.
 - DONE Put a default config file in the resource directory so it is bundled with uberjar, allowing user to run with no config file.
 - DONE Introduce log4j logging.  Consider slf4j?  http://www.slf4j.org/
 - DONE Change logging to redirect DEBUG only to the log file.  All other log statements go to both console and file.
 - DONE Allow program to print warning and continue if config files such as canadian_provinces.txt are not found.  This will help with distribution before I get those references correct.
 - DONE Be able to handle .gz files that are a single file, compressed as opposed to a group of files.
 - DONE include checks to verify that a regular expression in the config file is valid. Run these tests at startup, and report problems clearly to user.  
 - DONE sales_pipes.csv, line 6692.  For some reason the line wrap does not get recognized, so a bunch more lines get sucked up as part of that column.
 - DONE Add Cobertura coverage report.
 - DONE introduce config file (.properties format), read with commons-config.
 - DONE Start moving any hardcoded values to config file.
 - DONE introduce apache VFS
 - DONE Add mvn reports  checkstyle  pmd  cpd  findbugs  cobertura
 - DONE Add Clirr plugin to maven - reports API changes.
 - DONE Add support for recognizing boolean fields: 0,1,T,F,True,False,Yes,No,Y,N
 - DONE save column header, include in the results report.
 - DONE show column source (file name, folder, file name, etc) in report
 - DONE create patterns for US and canadian postal codes.
 - DONE introduce robust command line argument parsing.
 - DONE Find files inside ZIP files.
 - DONE Find files inside GZIP files.
 - DONE Process all files in folder.
 - DONE bundle up entire uberjar.
 - DONE externalize matching pattern so it is easy for users to customize
 - DONE Use config object for carrying all the parameters, instead of maintaining individual variables.
 - DONE Separate ProfileEngine.java into ProfileEngine.java (the API) and DataProfiler.java (the client app).
 - DONE Should handle blank lines in the file.
 - DONE shortest and lexicofirst should not include nulls.  
 - DONE Need to be able to tell if all values in a column are unique.  One option might be to dump them in a set, and count the members of the set.
 - DONE Allow a file containing a set of literal values.  This can be used for ISO codes, state and province names, etc.
 - DONE In ProfileEngine, rely only on Config object, instead of (legacy) individual passed in arguments.
 - DONE Complete moving all configuration to the configuration object, initialized from the .properties file.
 - DONE Support specifying multiple files as command line arguments.


# FIXED BUGS #
 - BUG: Don't compute correlation if the file contains only a single "data point / valid row".  Will throw an exception:
Caused by: org.apache.commons.math.MathRuntimeException$4: insufficient dimension 1, must be at least 2
    at org.apache.commons.math.MathRuntimeException.createIllegalArgumentException(MathRuntimeException.java:394)
    at org.apache.commons.math.stat.correlation.PearsonsCorrelation.correlation(PearsonsCorrelation.java:233)
