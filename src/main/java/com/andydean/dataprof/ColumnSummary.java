package com.andydean.dataprof;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


//TODO  Should also collect frequency of cell length, so fields that are all the same length can be determined (this are often codes - state abbrev, zip code, etc.
//      This is an extension of max and min length, perhaps.

public class ColumnSummary {

    // TODO Make all these members private eventually, create getters, etc.
    public String header;
    public String fileSource;
    public Map<String, Integer> typesMap = null;
    public int maxLength;
    public String longestCell;
    public int minLength;
    public String shortestCell;
    public long numValues;
    public long numEmpty;
    public String lexicoFirst;
    public String lexicoLast;
    public double numericLowest;
    public String numericLowestCell;
    public double numericHighest;
    public String numericHighestCell;

    // If the __OVERFLOW__ entry is missing then frequency contains all the
    // unique values in the column.
    public Map<String, Integer> frequency;
    public int numUniqueValues;

    private final static Logger logger = LoggerFactory.getLogger(ColumnSummary.class);

    ColumnSummary() {
        this.typesMap = new HashMap<String, Integer>();
        this.maxLength = 0;
        this.longestCell = "";
        this.minLength = Integer.MAX_VALUE;
        this.shortestCell = "";
        this.numValues = 0;
        this.numEmpty = 0;

        // char[] chars = new char[10];
        // Arrays.fill(chars, '\uFFFF');
        this.lexicoFirst = new String(new char[] { '\uFFFF', '\uFFFF', '\uFFFF', '\uFFFF' });
        this.lexicoLast = "";

        this.frequency = new HashMap<String, Integer>();
        this.numericLowest = Double.MAX_VALUE;
        this.numericLowestCell = "";
        this.numericHighest = Double.MIN_VALUE;
        this.numericHighestCell = "";
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getHeader() {
        return this.header;
    }

    public void setFileSource(String header) {
        this.fileSource = header;
    }

    public String getFileSource() {
        return this.fileSource;
    }

    public boolean isPrimaryKey() {
        // TODO Should not allow float value columns to be identified as primary
        // key.
        if (this.numEmpty == 0 && (this.numValues == this.numUniqueValues)) {
            return true;
        } else {
            return false;
        }
    }

    // TODO Make this a more accurate algorithm. This will depend on how the max
    // value (currently 10)
    // is set for incrementMap() call.
    public boolean isClassification() {
        long numNonEmpty = this.numValues - this.numEmpty;
        if (!this.frequency.containsKey("__OVERFLOW__") || this.frequency.get("__OVERFLOW__") < (numNonEmpty * 0.10)) {
            return true;
        } else {
            return false;
        }
    }

    public String isDate() {

        if (this.percentOfType("pattern.date.yyyy-mm-dd.regex") > 0.90) {
            return "date yyyy-mm-dd";
        }
        if (this.percentOfType("pattern.date.yyyymmdd.regex") > 0.90) {
            return "date yyyymmdd";
        }
        if (this.percentOfType("pattern.date.usa_format.regex") > 0.90) {
            return "date mm/dd/yyyy";
        }
        // TODO This is very fragile because the name has to match what is in dataprof.properties.
        if (this.percentOfType("pattern.datetime.usa_yy_format.regex") > 0.90) {
            return "date mm/dd/yy HH:MM";
        }
        
        if (this.percentOfType("pattern.date.usa_yyyy_format.regex") > 0.90) {
            return "date mm/dd/yyyy";
        }
        
        if (this.percentOfType("pattern.date.usa_yy_format.regex") > 0.90) {
            return "date mm/dd/yy";
        }
        
        

        return "";
    }
    
    public boolean isNumeric(ProfilerConfig config) {
        if ( this.guessDataType(config).getType().equals("number")) {
            return true;
        } else {
            return false;
        }
    }

    public float percentOfType(String type) {
        if (!this.typesMap.containsKey(type)) {
            return (float) 0.0;
        } else {
            float numNonEmpty = this.numValues - this.numEmpty;
            float num = this.typesMap.get(type);
            // System.out.println( "pct for " + type + " is " +
            // (num/numNonEmpty));
            return (num / numNonEmpty);
        }
    }
    


    public float getNullDensity() {
        return (float) this.numEmpty / (float) this.numValues;

    }

    public float getDensity() {
        return (1.0f - this.getNullDensity());
    }

    public void printSummary() {

        System.out.println("==========================");
        System.out.println(String.format("file source: %s", this.getFileSource()));
        System.out.println(String.format("header: %s", this.getHeader()));

        System.out.println(String.format("numValues: %d", this.numValues));
        System.out.println(String.format("numEmpty: %d", this.numEmpty));

        float pctNull = this.getNullDensity();
        System.out.println(String.format("null density: %5.2f", pctNull));

        System.out.println(String.format("numUniqueValues: %d", this.numUniqueValues));

        long numNonEmpty = this.numValues - this.numEmpty;

        // TODO If 90% of the values match a date, consider the column a date
        // and don't print anything else.

        // pattern.date.usa_format
        // pattern.date.yyyy-mm-dd

        for (String key : this.typesMap.keySet()) {
            int numDates = 0;
            if (key.startsWith("pattern.date.")) {
                numDates = numDates + this.typesMap.get(key);
            }

            if ((numNonEmpty * 0.90) < numDates) {
                System.out.println("Field type: Dates");
                return;
                // TODO Should show date specific extrema, which particular date
                // format, etc)
            }
        }

        // TODO If 90% of the values are included in frequency hash, consider
        // it a classification field, don't print anything else.

        // If __OVERFLOW__ doesn't exist, or it does but count is lower than
        // 10%, ...

        if (this.isClassification()) {
            System.out.println("Field type: Classification");

            System.out.println("Values and frequencies:");
            for (String classification : this.frequency.keySet()) {
                System.out.println(String.format("%s: %d", classification, this.frequency.get(classification)));
            }
            return;
        }

        // Unique identifier column can't have any null values.
        if (this.isPrimaryKey()) {
            // TODO Might not want to do this for floating point numbers.
            System.out.println("Field type: Unique Identifier");
            return;
        }

        // If 90% are signed floats, consider it a number.
        // TODO Replace with using guessDataType() method.
        if (this.percentOfType("pattern.number.signed_float.regex") > 0.90) {
            System.out.println("Field type: signed float");
            System.out.println("Min value = " + this.numericLowestCell);
            System.out.println("Max value = " + this.numericHighestCell);
            return;
        }

        // TODO If 90% of the values are True/False, print and stop.

        System.out.println("==========================");

        this.printDetails();

    }

    private String ellipsize(String input, int maxLength) {
        String ellip = "...";
        if (input == null || input.length() <= maxLength || input.length() < ellip.length()) {
            return input;
        }
        return input.substring(0, maxLength - ellip.length()).concat(ellip);
    }

    // Default version if length is not specified.
    private String ellipsize(String input) {
        return this.ellipsize(input, 13);
    }

    public static String getOneLinerHeader(ProfilerConfig config) {
        String s;
        //s = String.format("%-25s %-3s %-28s %5s %-3s %6s %7s %-25s %-25s", "Column", "PK?", "type", "Width", "FW?",
        s = String.format(config.onelinerHeaderFormat, "Column", "PK?", "type", "Width", "FW?",
                "Unique", "Density", "Min", "Max");
        return s;
    }

    public String getOneLiner(ProfilerConfig config) {
        String s;
        ColumnType type = this.guessDataType(config);
        // Next line shows | at the beginning of each line of outputs, for
        // making alignment easier.
        // | | | |
        //s = String.format("%-25s %-3s %-28s %5d %-3s %6d %7.3f %-25s %-25s", this.getHeader(),
        s = String.format(config.onelinerValuesFormat, this.getHeader(),
                (this.isPrimaryKey() ? "PK" : "-"), type.toString(), this.longestCell.length(), this.isFixedWidth() ? "T" : "F",
                this.numUniqueValues, this.getDensity(), this.ellipsize(this.guessRangeMin(type.getSubtype()), config.onelinerMinWidth),
                this.ellipsize(this.guessRangeMax(type.getSubtype()),config.onelinerMaxWidth));
        return s;
    }

    
    public boolean isFixedWidth() {
        if (this.shortestCell.length() == this.longestCell.length()) {
            return true;
        } else {
            return false;
        }
    }

    // TODO This might be deprecated - use isFixedWdth or
    // this.longestCell.length()
    public String getPrecision() {
        String s;
        if (this.shortestCell.length() == this.longestCell.length()) {
            s = String.format("fixed-width(%d)", this.longestCell.length());
        } else {
            s = String.format("%d", this.longestCell.length());
        }
        return s;
    }

    public String guessRangeMin(String type) {
        String s;
        if (this.numValues == this.numEmpty) {
            s = "";
        } else if (type.contains("integer")) {
            s = String.format("%s", this.numericLowestCell);
        } else if (type.contains("float")) {
            s = String.format("%s", this.numericLowestCell);
        } else {
            s = String.format("%s", this.lexicoFirst);
        }
        return s;
    }

    public String guessRangeMax(String type) {
        String s;
        if (this.numValues == this.numEmpty) {
            s = "";
        } else if (type.contains("integer")) {
            s = String.format("%s", this.numericHighestCell);
        } else if (type.contains("float")) {
            s = String.format("%s", this.numericHighestCell);
        } else {
            s = String.format("%s", this.lexicoLast);
        }
        return s;
    }

    // data type should return type and some info about the values.
    public ColumnType guessDataType(ProfilerConfig config) {
        long numNonEmpty = this.numValues - this.numEmpty;
        String type;
        type = "";

        if ( numNonEmpty == 0 ) {
            return new ColumnType( "text", "empty");
        }
        
        if ( this.numEmpty == 0 && this.numUniqueValues == 1) {
            return new ColumnType( "text", "constant");
        }

        // TODO The types such as "boolean" should be enums so they can be
        // compared elsewhere without doing string comparisons/
        if (percentOfType("pattern.logical.truefalse.regex") > 0.90) {
            type = "boolean";
            return new ColumnType( "boolean", "boolean" );
        }

        if (this.numUniqueValues == 2) {
            type = "binary";
            return new ColumnType("binary", "binary");
        }

        String dateType = this.isDate();
        if (!dateType.equals("")) {
            type = dateType;
            return new ColumnType("date", dateType);
        }

        if (this.percentOfType("pattern.text.website.regex") > 0.90) {
            type = "www";
            return new ColumnType("text", type);
        }
        
        if (this.percentOfType("pattern.text.firstnames.regex") > 0.20) {
            type = "personname";
            return new ColumnType("text", type);
        }
        

        // TODO Need to limit this to 1 or 2 words (limit the number of spaces).  Otherwise matches to much arbitrary text as well.
        //      Do this by creating another regex pattern that matches up to three strings, and check the percentage of those as well.
        if (this.percentOfType("pattern.text.townnames.regex") > 0.20) {
            type = "cityname";
            return new ColumnType("text", type);
        }
        
        
        if (this.percentOfType("pattern.text.usa_state_abbrev.filename") > 0.90) {
            type = "stateabbrev";
            return new ColumnType("text", type);
        }

        if (this.percentOfType("pattern.text.canadian_provinces.filename") > 0.70) {
            type = "canadaprovincename";
            return new ColumnType("text", type);
        }
        
        

        // It is important that unsigned is handled before signed.

        if (this.percentOfType("pattern.number.unsigned_integer.regex") > 0.90) {
            if (this.numericLowest >= 0.0) {
                type = "unsigned integer";
                return new ColumnType("number", type);
            }
        }

        if (this.percentOfType("pattern.number.signed_integer.regex") > 0.90) {
            type = "signed integer";
            return new ColumnType("number", type);
        }

        if (this.percentOfType("pattern.number.unsigned_float.regex") > 0.90) {
            if (this.numericLowest >= 0.0) {
                type = "unsigned float";
                return new ColumnType("number", type);
            }
        }

        if (this.percentOfType("pattern.number.signed_float.regex") > 0.90) {
            type = "signed float";
            return new ColumnType("number", type);
        }

        if (this.isClassification()) {
            type = "text/classification";
            return new ColumnType("text", "classification");
        }

        type = "text";
        return new ColumnType("text", "text");
    }

    public void printDetails() {
        System.out.println(String.format("file source: %s", this.getFileSource()));
        System.out.println(String.format("header: %s", this.getHeader()));

        System.out.println(String.format("numValues: %d", this.numValues));
        System.out.println(String.format("numEmpty: %d", this.numEmpty));

        double pctNull = (double) this.numEmpty / (double) this.numValues;
        System.out.println(String.format("null density: %5.2f", pctNull));

        System.out.println(String.format("numUniqueValues: %d", this.numUniqueValues));

        System.out.println(String.format("max Length: %d", this.maxLength));
        System.out.println(String.format("longest: %s", this.longestCell));

        System.out.println(String.format("min Length: %d", this.minLength));
        System.out.println(String.format("shortest: %s", this.shortestCell));

        // TODO Use shortest == longest when determining type. Such fields are
        // likely codes.

        System.out.println(String.format("lexicoFirst: %s", this.lexicoFirst));
        System.out.println(String.format("lexicoLast: %s", this.lexicoLast));

        System.out.println(String.format("numericLowest: %s", this.numericLowest));
        System.out.println(String.format("numericLowestCell: %s", this.numericLowestCell));

        System.out.println(String.format("numericHighest: %s", this.numericHighest));
        System.out.println(String.format("numericHighestCell: %s", this.numericHighestCell));

        System.out.println("Subset of unique values with frequency:");
        for (String key : this.frequency.keySet()) {
            System.out.println(String.format("%s: %d", key, this.frequency.get(key)));
        }

        System.out.println("Matched types:");
        for (String key : this.typesMap.keySet()) {
            System.out.println(String.format("%s: %d", key, this.typesMap.get(key)));
        }

    }

}
