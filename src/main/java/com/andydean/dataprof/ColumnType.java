package com.andydean.dataprof;

public class ColumnType {

    private String type;
    private String subtype;

    public ColumnType(String type, String subtype) {
        this.type = type;
        this.subtype = subtype;
    }

    public String getType() {
        return this.type;
    }

    public String getSubtype() {
        return this.subtype;
    }
    
    public String toString() {
        return this.type + "/" + this.subtype;
    }

}
