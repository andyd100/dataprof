package com.andydean.dataprof;

public class TypePattern {

    private String name;
    private String regex;

    public TypePattern(String name, String regex) {
        this.name = name;
        this.regex = regex;
    }

    public String getRegex() {
        return this.regex;
    }

    public String getName() {
        return this.name;
    }

}
