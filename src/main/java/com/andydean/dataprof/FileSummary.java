package com.andydean.dataprof;

import java.util.List;
import java.util.ArrayList;

public class FileSummary {

    // TODO Make all these members private eventually, create getters, etc.
    public String fileName;
    public String fileSource;
    public String fileType;
    public String columnSeparator;
    public int numLines;
    public int numColumns;
    public List<ColumnSummary> columnSummaries;
    public double[][] corrPearson;
    public double[][] corrSpearmans;
    public List<String> fileTopRows;

    FileSummary() {
        this.numLines = 0;
        this.numColumns = 0;
        this.columnSummaries = new ArrayList<ColumnSummary>();
        this.corrPearson = null;   // Initialize empty for now.  Will be allocated later.
        this.corrSpearmans = null;
        this.fileTopRows = new ArrayList<String>();
    }

    public void printSummary() {

        System.out.println("==========================");
        System.out.println(String.format("file source: %s", this.fileSource));
        System.out.println(String.format("file name: %s", this.fileName));

        System.out.println(String.format("file type: %s", this.fileType));
        System.out.println(String.format("column separator: %s", this.columnSeparator));
    }

}
