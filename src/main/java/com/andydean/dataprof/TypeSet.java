package com.andydean.dataprof;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TypeSet {

    private String name;
    private Set<String> setOfValues;
    private static final Logger logger = LoggerFactory.getLogger(TypeSet.class);

    public TypeSet(String name, String filename) throws IOException {
        this.name = name;

        this.setOfValues = new HashSet<String>();
        // Load lines from filename into set
        // Could also do it like
        // http://stackoverflow.com/questions/5804269/text-file-into-java-setstring-using-commons-or-guava
        // but I would like to allow # comments in data files.
        BufferedReader br;
        try {
            br = new BufferedReader(new FileReader(filename));
        } catch (Exception e) {
            // If the file is not found, log the error but continue processing.
            logger.warn("types file '{}' for type '{}' could not be found.", filename, name);
            return;
        }
        String line;
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) {
                continue; // Skip to next line in the file.
            }
            if (line.trim().length() == 0) {
                continue; // Skip to next line in the file.
            }
            this.setOfValues.add(line.trim());
        }
    }

    public Set<String> getSetOfValues() {
        return this.setOfValues;
    }

    public String getName() {
        return this.name;
    }

}
