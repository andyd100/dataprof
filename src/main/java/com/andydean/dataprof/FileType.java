package com.andydean.dataprof;

public enum FileType {
    CSV_FILE,
    XLS_FILE,
    XLSX_FILE,
    ZIP_FILE,
    TAR_FILE,
    JAR_FILE,
    TGZ_FILE,
    TXT_FILE,
    GZ_FILE,
    DIRECTORY,
    UNKNOWN_FILE_TYPE

}
