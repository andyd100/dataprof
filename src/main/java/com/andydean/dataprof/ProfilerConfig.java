package com.andydean.dataprof;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

//TODO Change to use getters instead of public methods

public class ProfilerConfig {

    public int defaultSampleFrequency = 1;
    public int maxLinesToDetermineFileSeparator = 1000;
    public int sampleSize = 10000000;
    public char quoteChar;
    public char separatorChar;
    public int sampleFrequency;
    public String onelinerHeaderFormat;
    public String onelinerValuesFormat;
    public int onelinerMaxWidth;
    public int onelinerMinWidth;


    public List<String> inputFiles;

    /* List of regular expression patterns to try to match. */
    private List<TypePattern> patternList;

    /* List of javascript expression patterns to try to match. */
    private List<TypeExpr> exprList;

    /* List of sets to try to match. */
    private List<TypeSet> setList;

    private ProfilerConfig() {
        this.patternList = new ArrayList<TypePattern>();
        this.setList = new ArrayList<TypeSet>();
        this.exprList = new ArrayList<TypeExpr>();
        this.inputFiles = new ArrayList<String>();
    }

    public ProfilerConfig(String configFile) throws IOException, URISyntaxException {
        this();

        PropertiesConfiguration config;

        // This ctor reads the config file twice -once to read all the pattern.*
        // entries, and then another time to
        // read particular settings like sampleFrequency or the quoteChar.
        // This should be broken up to separate the patterns (which are probably
        // generally static and can be
        // less frequently edited by a user) and the other settings.
        //

        // File f = new File(this.getClass().getResource(configFile).toURI());

        loadPatterns(configFile);

        try {
            // config = new PropertiesConfiguration(configFile);
            config = new PropertiesConfiguration();

            InputStream in = getClass().getResourceAsStream(configFile);
            // BufferedReader reader = new BufferedReader(new
            // InputStreamReader(in));
            config.load(in);

            this.defaultSampleFrequency = config.getInt("default.sampleFrequency", 1);
            this.maxLinesToDetermineFileSeparator = config.getInt("maxLinesToDetermineFileSeparator");
            this.sampleSize = config.getInt("sampleSize");
            this.sampleFrequency = config.getInt("sample_frequency", 1);

            if ("DQUOTE".equals(config.getString("quoteChar"))) {
                this.quoteChar = '"';
            } else {
                String temp = config.getString("quoteChar", "__KEY_DOES_NOT_EXIST__");
                if ("__KEY_DOES_NOT_EXIST__".equals(temp)) {
                    this.quoteChar = '\0';
                } else {
                    this.quoteChar = temp.charAt(0);
                }
            }

            // TODO Need to read separatorChar


            this.onelinerHeaderFormat = config.getString("oneliner.header.format");
            this.onelinerValuesFormat = config.getString("oneliner.values.format");
            this.onelinerMinWidth = config.getInt("oneliner.values.min.width", 10);
            this.onelinerMaxWidth = config.getInt("oneliner.values.max.width", 10);


            // TODO Is there a better way to handle errors within the ctor.
        } catch (ConfigurationException e) {
            System.err.println("Error reading configuration file");
            e.printStackTrace(System.err);
            System.exit(1);
            // this.defaultSampleFrequency = 1;
        }

    }

    public List<TypePattern> getPatternList() {
        return this.patternList;
    }

    public List<TypeExpr> getExprList() {
        return this.exprList;
    }

    public List<TypeSet> getSetList() {
        return this.setList;
    }

    private void loadPatterns(String configFile) throws IOException {
        // TODO Might need some sort of default pattern that everything matches.

        PropertiesConfiguration config;
        try {
            // File f = new
            // File(this.getClass().getResource(configFile).toURI());

            // config = new PropertiesConfiguration(configFile);
            config = new PropertiesConfiguration();

            // Disable delimiter parsing, because regular expressions may
            // include commas.
            config.setDelimiterParsingDisabled(true);

            InputStream in = getClass().getResourceAsStream(configFile);
            // BufferedReader reader = new BufferedReader(new
            // InputStreamReader(in));
            config.load(in);

            // Disable delimiter parsing, because regular expressions may
            // include commas.
            // config.setDelimiterParsingDisabled(true);
            // config.refresh();

            // Load all keys with the prefix "pattern"
            Iterator<String> keys = config.getKeys("pattern");
            // List<String> keyList = new ArrayList<String>();
            String key;
            String regex;
            while (keys.hasNext()) {
                key = keys.next();
                if (key.contains(".regex")) {
                    // keyList.add(key);
                    regex = config.getString(key);

                    // TODO This is temporary, just to check if regex is valid.
                    // Eventually should use Matcher...
                    try {
                        Pattern pattern = Pattern.compile(regex);
                    } catch (PatternSyntaxException e) {
                        System.out.println("FATAL: Pattern for '" + key + "' is invalid");
                        System.out.println("FATAL: Pattern is '" + regex + "'");
                        e.printStackTrace();
                        Runtime.getRuntime().exit(1);
                    }
                    this.patternList.add(new TypePattern(key, regex));
                } else if (key.contains(".expr")) {
                    // TODO Here should try to compile the expression.
                    // TODO Here should handle the different scripting engines
                    // (javascript, groovy, etc)
                    String expr = config.getString(key).trim();
                    this.exprList.add(new TypeExpr(key, expr));
                } else if (key.contains(".filename")) {
                    String filename = config.getString(key);
                    this.setList.add(new TypeSet(key, filename));
                } else {
                    System.err.println("ERROR  Don't know how to handle patterns like " + key);
                }
            }
        } catch (ConfigurationException e) {
            System.out.println("ERROR: Problem reading patterns from config file. " + e.getMessage());
            e.printStackTrace();
        }
    }

}
