package com.andydean.dataprof;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;

import org.apache.commons.math.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math.stat.correlation.SpearmansCorrelation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;

import com.google.common.base.CharMatcher;
import com.google.common.io.Files;

/**
 * Data Profiler
 *
 */
public class ProfileEngine {

    private final static Logger logger = LoggerFactory.getLogger(ProfileEngine.class);

    public ProfileEngine() {

    }

    static void processDataFile(ProfilerConfig config, String inputFile, FileType fileType)
            throws FileNotFoundException, FileSystemException, IOException {

        FileSummary fileSummary = new FileSummary();

        Reader reader = ProfileEngine.getReader(inputFile);
        char separator = ProfileEngine.determineFileSeparator(reader, config.maxLinesToDetermineFileSeparator);
        if ( separator == '\0' ){
            logger.info( "Skipping {} because the column separator could not be determined.", inputFile);
            return;
        }
        reader.close();
        
        
        reader = ProfileEngine.getReader(inputFile);
        fileSummary.fileTopRows = ProfileEngine.readFileHeader(reader, 5);
        reader.close();


        logger.debug("separator is '{}'",  (separator == '\t' ? "TAB" : separator));
        logger.debug("sampleFrequency is {}", config.sampleFrequency);
        logger.debug("inputFile is {}",  inputFile);
        logger.debug("fileType is {}",  fileType);

        fileSummary.columnSeparator = (separator == '\t' ? "TAB" : String.valueOf(separator));
        // woof woof

        // TODO Extract the quote character as a configuration setting.
        // (Need to find a way to infer it.)
        // Using '\0 for quote character to prevent embedded quotes (for
        // inches, in default file) from being enforced.
        List<Column> columns = ProfileEngine.sampleFileIntoColumns(inputFile, separator, config.quoteChar,
                config.sampleSize, config.sampleFrequency);

        // Load the file summary from the file summary info of the first column:
        fileSummary.fileSource = columns.get(0).getFileSource();
        //fileSummary.fileName= columns.get(0).

        ProfileEngine pe = new ProfileEngine();

        List<ColumnSummary> columnSummaries = new ArrayList<ColumnSummary>();
        fileSummary.columnSummaries = columnSummaries;

        int i = 0;
        for (Column column : columns) {
            ColumnSummary cs = pe.analyzeColumn(config, column);

            //TODO Toggle this with a command line arg, or write it to a debug file.
            if( 1 == 2 ){
                System.out.println("\n\n ------------------------------\n");
                System.out.println("Column # " + i);
                cs.printSummary();
            }
            columnSummaries.add(cs);
            i++;
        }
        
        // TODO Now user the column data, plus the column summaries, to create correlation matrix 
        //      for all pairs of numeric columns (including dates) using
        //      http://commons.apache.org/proper/commons-math/javadocs/api-3.3/org/apache/commons/math3/stat/correlation/package-summary.html
        //
        //      Then output the results sorting from highest absolute value of correlation to lowest, since we don't have graphical output yet.
        //
        // TODO Move this into summarize()
        
        computeColumnCorrelations(config, fileSummary, columns, columnSummaries);

        // TODO Need to also handle file header info (separator, # of columns, etc)
        // TODO Need to be able to pass the summary info back up to a calling client.
        summarize(config, fileSummary, columnSummaries);
    }

    private static void computeColumnCorrelations( ProfilerConfig config, FileSummary fileSummary, List<Column> columns,
            List<ColumnSummary> columnSummaries) {
        fileSummary.corrPearson = new double[columns.size()][columns.size()];
        fileSummary.corrSpearmans = new double[columns.size()][columns.size()];
        
        for ( int ii = 0; ii < columns.size(); ii++ ){
            if ( columns.get(ii).getValues().size() <= 1) {
                break;  // If there is not more than 1 data value, can't compute correlation
            }
            for (int jj = 0; jj < ii; jj++ ) {
                if ( columns.get(jj).getValues().size() <= 1) {
                    break;  // If there is not more than 1 data value, can't compute correlation
                }
                ColumnSummary cs1 = columnSummaries.get(ii);
                ColumnSummary cs2 = columnSummaries.get(jj);
              
                if ( cs1.isNumeric(config) && cs2.isNumeric(config) ) {
                    PearsonsCorrelation pc = new PearsonsCorrelation();
                    SpearmansCorrelation sc = new SpearmansCorrelation();

                    double[] d0 = new double[columns.get(ii).getValues().size()];
                    double[] d1 = new double[columns.get(jj).getValues().size()];
                    int kk = 0;
                    for ( int k = 0; k < columns.get(jj).getValues().size(); k++){
                        // if one value is null or empty string, skip the other as well. 
                        if ( ! ( columns.get(ii).getValues().get(k).trim().isEmpty() || columns.get(ii).getValues().get(k).trim().isEmpty() ) ){
                            try {
								d0[kk] = Double.parseDouble(columns.get(ii).getValues().get(k));
								d1[kk] = Double.parseDouble(columns.get(jj).getValues().get(k));
								kk++;
							} catch (NumberFormatException e) {
								// Do nothing, but skip values that aren't legit numbers.
							}
                            
                        }
                    }
                    
                    //if ( kk >= 1 ) { // correlation requires at least 2 data points.
                        fileSummary.corrPearson[ii][jj] = pc.correlation(d0, d1); 
                        fileSummary.corrSpearmans[ii][jj] = sc.correlation(d0, d1);
                    //}
                }
            }
        }
    }
    
    


    public static void summarize(ProfilerConfig config, FileSummary fileSummary, List<ColumnSummary> colsumms) {

        System.out.println("+++++++    ++++++++++    ++++++++++++");
        

        System.out.println(fileSummary.columnSummaries.get(0).getFileSource());

        //TODO print file summary
        //fileSummary.
        System.out.println( String.format( "Source: %s", fileSummary.fileSource ));
        System.out.println( String.format( "Name: %s", fileSummary.fileName ));
        System.out.println( String.format( "Separator: '%s'", fileSummary.columnSeparator ));
        System.out.println( String.format( "# Columns: '%s'", fileSummary.columnSummaries.size() ));

        System.out.println("");

        for ( String s: fileSummary.fileTopRows ) {
            System.out.println(s);
        }

        System.out.println("");

        int line = 1;
        System.out.println(String.format(" %4s %s", "Col#", ColumnSummary.getOneLinerHeader(config)));
        System.out.println(String.format(" %4s %s", "    ", ColumnSummary.getOneLinerHeader(config).replaceAll("[a-zA-Z?]", "-")));
        for (ColumnSummary cs : fileSummary.columnSummaries) {
            System.out.println(String.format(" %3d: %s", line++, cs.getOneLiner(config)));
        }
        
        System.out.println( "Column x Column Correlations");
        // TODO Would be good to sort by the strength of the correlation.
        for ( int ii = 0; ii < colsumms.size(); ii++ ){
            for (int jj = 0; jj < ii; jj++ ) {
                ColumnSummary cs1 = colsumms.get(ii);
                ColumnSummary cs2 = colsumms.get(jj);
                if ( cs1.isNumeric(config) && cs2.isNumeric(config) ) {
                    System.out.println( String.format("Correlation between col '%-20s' and col '%-20s'    P= %+6f    S= %+6f", cs1.header, cs2.header, fileSummary.corrPearson[ii][jj], fileSummary.corrSpearmans[ii][jj] ));
                }
            }
        }

    }

    // Turn a VFS file into a Reader to be used for parsing.
    public static Reader getReader(String inputFile) throws FileNotFoundException, FileSystemException {
        Reader r;

        // If the file specified includes :, assume it is a URL and use apache
        // VFS.
        // Otherwise assume it is a naked file name.

        // TODO This will only work on Windows, and poorly there! Need something
        // more robust.


        //String ext = Files.getFileExtension(inputFile);
        //logger.debug( "ext is: {} for {}", ext, inputFile);

        //if (inputFile.contains(":")) {
        if (true) {
            FileSystemManager fsManager = VFS.getManager();
            FileObject inFile = fsManager.resolveFile(inputFile);
            //final FileObject file = mgr.resolveFile(arg);

            logger.debug("URL: {}", inFile.getURL());
            logger.debug("getName(): {}", inFile.getName());
            logger.debug("BaseName: {}", inFile.getName().getBaseName());
            logger.debug("Extension: {}", inFile.getName().getExtension());
            logger.debug("Path: {}", inFile.getName().getPath());
            logger.debug("Scheme: {}", inFile.getName().getScheme());
            logger.debug("URI: {}", inFile.getName().getURI());
            logger.debug("Root URI: {}", inFile.getName().getRootURI());
            logger.debug("Parent: {}", inFile.getName().getParent());
            logger.debug("Type: {}", inFile.getType());
            logger.debug("Exists: {}", inFile.exists());
            logger.debug("Readable: {}", inFile.isReadable());
            logger.debug("Writeable: {}", inFile.isWriteable());
            logger.debug("Root path: {}", inFile.getFileSystem().getRoot().getName().getPath());

            InputStream inStream = inFile.getContent().getInputStream();
            r = new InputStreamReader(inStream);
            return r;
        } else {
            // Handle as a regular file, instead of an Apache VFS file.
            // TODO I want to get rid of this branch, have everything consistently go through VFS instead.
            //      The downside to using VFS always is that VFS makes a copy of the file, which can be slow and waste space for very large input.

            File f = new File(inputFile);
            logger.debug("absolute path of {} is {}", inputFile, f.getAbsolutePath());
            //TODO If having the path include things like .. is a problem, switch to getCanonicalPath()

            r = new FileReader(inputFile);
            return r;
        }
    }

    public static char determineFileSeparator(Reader reader, int maxLinesToSample) throws IOException {
        ArrayList<Character> candidates = new ArrayList<Character>(Arrays.asList(',', '|', '\t', ';'));

        // Loop through the file. Counting occurrences of each candidates. Any
        // zeros should be eliminated.
        String line;
        BufferedReader br = new BufferedReader(reader);

        int linesRead = 1;
        while ((line = br.readLine()) != null && linesRead <= maxLinesToSample) {
            linesRead = linesRead + 1;
            if (line.trim().length() > 0) {
                for (int i = 0; i < candidates.size(); i++) {
                    if (CharMatcher.is(candidates.get(i)).countIn(line) == 0) {
                        logger.debug("line is {}", line);
                        logger.debug("Removing separator candidate '{}'.", candidates.get(i));
                        candidates.remove(i);
                    }
                }
            }
        }
        br.close();

        // TODO Need a more robust method, and also one which doesn't have to
        // read the entire file.

        // For now assume that separator is the only candidate which appears in
        // every line.
        if ( candidates.isEmpty()){
            return '\0';
        } else {
            return candidates.get(0);
        }
    }

    /** Read the top few rows of a file */
    public static List<String> readFileHeader(Reader reader, int maxLinesToSample) throws IOException {
        List <String> rows = new ArrayList<String>();

        // Loop through the file. Counting occurrences of each candidates. Any
        // zeros should be eliminated.
        String line;
        BufferedReader br = new BufferedReader(reader);

        int linesRead = 1;
        while ((line = br.readLine()) != null && linesRead <= maxLinesToSample) {
            linesRead = linesRead + 1;
            rows.add(line);
        }
        br.close();
        return rows;
    }

    
    public static FileType determineFileType(String file) {
        File f = new File(file);

        //TODO Can use FileSystemManager.getSchemes() to know what schemes Apache VFS supports.

        logger.debug("file is {}", f);
        logger.debug("url is {}", f.toURI());

        if (f.isDirectory()) {
            return FileType.DIRECTORY;
        }

        String ext = Files.getFileExtension(file);

        if (ext.equalsIgnoreCase("XLS")) {
            return FileType.XLS_FILE;
        } else if (ext.equalsIgnoreCase("XLSX")) {
            return FileType.XLSX_FILE;
        } else if (ext.equalsIgnoreCase("CSV")) {
            return FileType.CSV_FILE;
        } else if (ext.equalsIgnoreCase("TXT")) {
            return FileType.TXT_FILE;
        } else if (ext.equalsIgnoreCase("ZIP")) {
            return FileType.ZIP_FILE;
        } else if (ext.equalsIgnoreCase("GZ")) {
            return FileType.GZ_FILE;
        } else if (ext.equalsIgnoreCase("TGZ")) {
            return FileType.TGZ_FILE;
        } else if (ext.equalsIgnoreCase("TAR")) {
            return FileType.TAR_FILE;
        } else if (ext.equalsIgnoreCase("JAR")) {
            return FileType.JAR_FILE;
        }

        return FileType.TXT_FILE;
        //TODO Might make this more picky later.  For now, assume unrecognized files are text files.
        //return FileType.UNKNOWN_FILE_TYPE;
    }

    public ColumnSummary analyzeColumn(ProfilerConfig config, Column column) {
        ColumnSummary cs = new ColumnSummary();

        Set<String> uniqueValues = new HashSet<String>();

        cs.setHeader(column.getHeader());
        cs.setFileSource(column.getFileSource());

        // TODO These names TypePattern, TypeSet, are not very good. Improve?
        List<TypePattern> listOfRegexPatterns = config.getPatternList();
        List<TypeSet> listOfSetPatterns = config.getSetList();
        List<TypeExpr> listOfExprPatterns = config.getExprList();

        Double d;

        // TODO Should allow user to specify which engine to use.
        // TODO Try to leverage reusable parts of formulas project.
        // TODO This is very slow!
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");

        // Loop over all the entries of a column
        for (String cell : column.getValues()) {
            // System.out.println(cell);

            cs.numValues = cs.numValues + 1;

            int len = cell.length();

            if (len == 0) {
                cs.numEmpty++;
                // Stop processing if the cell is determined to be empty.
                continue;
            }

            // TODO If memory is a concern, might not save for values that look
            // like
            // floating point numbers with relatively large number of
            // significant
            // digits or which are really long text.
            uniqueValues.add(cell);

            if (len > cs.maxLength) {
                cs.maxLength = len;
                cs.longestCell = cell;
            }

            if (len < cs.minLength) {
                cs.minLength = len;
                cs.shortestCell = cell;
            }

            if (cell.compareTo(cs.lexicoFirst) < 0) {
                cs.lexicoFirst = cell;
            }

            if (cell.compareTo(cs.lexicoLast) > 0) {
                cs.lexicoLast = cell;
            }

            try {
                d = Double.parseDouble(cell);
                if (d < cs.numericLowest) {
                    cs.numericLowest = d;
                    cs.numericLowestCell = cell;
                }
                if (d > cs.numericHighest) {
                    cs.numericHighest = d;
                    cs.numericHighestCell = cell;
                }
            } catch (NumberFormatException e) {

            }

            // Count frequency of the first 10 unique values in the column.
            // This is used for recognizing fields which are classifications.
            // TODO Make the number of elements in histogram a config setting.
            incrementMap(cs.frequency, cell, 20);  // woof changed from 10 to 20

            for (int i = 0; i < listOfRegexPatterns.size(); i++) {
               
                if (cell.matches(listOfRegexPatterns.get(i).getRegex())) {
                	//System.out.print( "match ");
                	//System.out.println(listOfRegexPatterns.get(i).getRegex());
                    incrementMap(cs.typesMap, listOfRegexPatterns.get(i).getName(), 1000);
                }
            }

            // TODO Should only do this if there are expressions to evaluate
            for (int i = 0; i < listOfExprPatterns.size(); i++) {
                // System.out.println( "Comparing with the expression " +
                // listOfExprPatterns.get(i).getExpr());
                engine.put("cell_value", cell);
                try {
                    Boolean rv = (Boolean) engine.eval(listOfExprPatterns.get(i).getExpr());
                    // System.out.println( "return value: " + rv );
                    if (rv) {
                        incrementMap(cs.typesMap, listOfExprPatterns.get(i).getName(), 1000);
                    }
                } catch (ScriptException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            for (int i = 0; i < listOfSetPatterns.size(); i++) {
                // TODO Should this comparison be case-sensitive - or should
                // that option be exposed to user?
                if (listOfSetPatterns.get(i).getSetOfValues().contains(cell)) {
                    incrementMap(cs.typesMap, listOfSetPatterns.get(i).getName(), 1000);
                }
            }
        }
        cs.numUniqueValues = uniqueValues.size();
        return cs;
    }

    // TODO Define a sampling strategy object.

    // TODO Do some validation that the number of columns is the same in every
    // row

    public static boolean guessIfColumnHeaders(String[] line) {
        for (String cell : line) {
            if (!cell.matches("^[a-zA-Z].*$")) {
                if( cell.trim().isEmpty()){
                    logger.debug("column header is missing");
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /* Read a very well behaved CSV file, for testing */
    public static List<Column> sampleFileIntoColumns(String file, char sep, char quote_char, int maxSampleSize,
            int sampleFrequency) throws IOException {
        // TODO Should change this to take a ProfilerConfig instead of
        // individual scalar arguments.
        List<Column> listOfColumns;

        // Use this to skip saving data, to make column checks run faster.
        // Need to formalize this as a feature eventually.
        boolean saveData = true;

        listOfColumns = new ArrayList<Column>();

        Reader rawReader = ProfileEngine.getReader(file);

        CSVReader reader = new CSVReader(rawReader, sep, quote_char);
        String[] nextLine;

        // Assume first line is headers. Read it, use it to determine the number
        // of columns in the file, but discard the values.
        nextLine = reader.readNext();

        boolean firstRowIsColumnNames = ProfileEngine.guessIfColumnHeaders(nextLine);
        // Create a column for each column of input
        int colNum = 1;
        for (String header : nextLine) {
            String colName;
            if (firstRowIsColumnNames) {
                colName = header;
                if ( colName.trim().isEmpty()){
                    colName = "Column " + colNum;
                }
            } else {
                colName = "Column " + colNum;
            }
            Column col = new Column(colName, file);
            listOfColumns.add(col);
            colNum++;
        }

        // TODO Need a way to guess whether or not the first row is a header.
        // One way might be of all of the values in row 1 begin with a letter.
        // That will work if any columns are numeric.

        logger.debug("Based on first line, looks like column count = {}", listOfColumns.size());

        int linesRead = 1;

        while (((nextLine = reader.readNext()) != null) && (linesRead < maxSampleSize)) {
            // Check if this was an empty line.
            if (nextLine.length == 1 && nextLine[0].isEmpty()) {
                logger.debug("read an empty line");
                continue;
            }

            // nextLine[] is an array of values from the line
            if (nextLine.length > listOfColumns.size()) {
                // Another way to do this might be to just capture the
                // IndexOutOfBoundsException ...
                // TODO Should consider whether line should be skipped, column
                // count should be expanded, separator changed and restart, stop
                // parsing, etc.
                String fmt = "WARNING Unexpected number of columns.  Expected '%d', Actual '%d'.  Line will be skipped.";
                String msg = String.format(fmt, listOfColumns.size(), nextLine.length);
                System.out.println(msg);
                fmt = "linenum: '%d', line: '%s'";
                msg = String.format(fmt, linesRead, StringUtils.join(nextLine, sep));
                System.err.println(msg);
            }
            if (0 == (linesRead % 10000)) {
                logger.debug("[{}]: {}", linesRead, StringUtils.join(nextLine, sep));
            }
            if (0 == (linesRead % sampleFrequency)) {
                if (saveData) {
                    // TODO Don't save the value if the column count is wrong -
                    // it may
                    // mess up other stats. Do count the number of such rows,
                    // though.
                    for (int i = 0; i < Math.min(nextLine.length, listOfColumns.size()); i++) {
                        String val = nextLine[i].trim();
                        listOfColumns.get(i).add(val);
                    }
                }
            }
            linesRead++;
        }
        reader.close();
        return listOfColumns;
    }

    // TODO create a version that doesn't require maxSize to be passed in.
    private static void incrementMap(Map<String, Integer> map, String key, int maxSize) {
        if (map.containsKey(key)) {
            Integer val = map.get(key);
            val = val + 1;
            map.put(key, val);
        } else if (map.size() < maxSize) {
            map.put(key, 1);
        } else {
            // This can be used to determine if there were values beyond the
            // maxSize limit.
            // map.put("__OVERFLOW__", 1);
            incrementMap(map, "__OVERFLOW__", Integer.MAX_VALUE);
        }
    }
};
