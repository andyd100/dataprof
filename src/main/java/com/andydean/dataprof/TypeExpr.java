package com.andydean.dataprof;

public class TypeExpr {

    private String name;
    private String expr;

    public TypeExpr(String name, String expr) {
        this.name = name;
        this.expr = expr;
    }

    public String getExpr() {
        return this.expr;
    }

    public String getName() {
        return this.name;
    }

}
