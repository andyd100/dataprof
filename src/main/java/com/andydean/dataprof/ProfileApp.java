package com.andydean.dataprof;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import java.nio.file.Paths;


import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Files;

public class ProfileApp {

    private final static Logger logger = LoggerFactory.getLogger(ProfileApp.class);

    // mvn3 exec:java -Dexec.mainClass="com.andydean.dataprof.ProfileApp"

    public static void main(String[] args) throws IOException, URISyntaxException {

        logger.info("\n\n==================  new run ==============================\n\n");

        logger.debug( "Supported schemes:");
        for (String scheme: VFS.getManager().getSchemes()){
            logger.debug("scheme: {}", scheme);
        }

        List<FileSummary> fileSummaries;
        fileSummaries = new ArrayList<FileSummary>();

        long startTime = System.nanoTime();

        ProfilerConfig config = processConfiguration(args);

        // TODO Maybe quickly count the lines in the file and then use that
        // to compute sample frequency.

        for (String inputFile : config.inputFiles) {
            //TODO determineFileType should consider both the file extension and the
            //     URL scheme, if it is included.
            FileType fileType = ProfileEngine.determineFileType(inputFile);

            if (fileType.equals(FileType.CSV_FILE) || fileType.equals(FileType.TXT_FILE)) {
                File f = new File(inputFile);
                String fcp = f.getCanonicalPath();

                ProfileEngine.processDataFile(config, fcp, fileType);
                // TODO processDataFile should return a FileSummary that can be
                // appended to a list of file summaries, which get printed at
                // the end.

            } else if (ProfileApp.isVfsSupportedScheme(inputFile)){
                // Locate the Jar file
                FileSystemManager fsManager = VFS.getManager();

                String f = generateVfsUrl(inputFile);
                FileObject jarFile = fsManager.resolveFile(f);

                // List the children of the Jar file
                FileObject[] children = jarFile.getChildren();
                System.out.println("Children of " + jarFile.getName().getURI());
                for (int i = 0; i < children.length; i++) {
                    System.out.println(children[i].getName().getFriendlyURI());
                    System.out.println(children[i].getName().getPath());
                    System.out.println(children[i].getName().getBaseName());
                    System.out.println("");

                    ProfileEngine.processDataFile(config, children[i].getName().getFriendlyURI(), fileType);
                    // TODO processDataFile should return a FileSummary that can
                    // be appended to a list of fileSummaries, which get printed
                    // at the end.
                }
            } else if (fileType.equals(FileType.XLS_FILE) || fileType.equals(FileType.XLSX_FILE)) {
                System.err.println("Don't know how to analyze Excel files of type " + fileType);

            } else if (fileType.equals(FileType.DIRECTORY)) {
                // TODO Need to improve this crude and limited way to recurse
                // the file system.
                File folder = new File(inputFile);
                File[] listOfFiles = folder.listFiles();

                for (int i = 0; i < listOfFiles.length; i++) {
                    if (listOfFiles[i].isFile()) {
                        System.out.println("File " + listOfFiles[i].getName());
                        System.out.println("Canonical File " + listOfFiles[i].getCanonicalPath());

                        FileType ft = ProfileEngine.determineFileType(listOfFiles[i].getCanonicalPath());
                        if (ft.equals(FileType.CSV_FILE) || ft.equals(FileType.TXT_FILE)) {
                            logger.debug("processing data file '{}'", listOfFiles[i].getCanonicalPath());
                            ProfileEngine.processDataFile(config, listOfFiles[i].getCanonicalPath(), ft);
                        } else {
                            System.out.println("Skipping non-csv, non-text file " + listOfFiles[i].getCanonicalPath());
                        }
                    } else if (listOfFiles[i].isDirectory()) {
                        System.out.println("Directory " + listOfFiles[i].getName());
                    }
                }
            } else {
                System.err.println("Don't know how to analyze files of type " + fileType);
            }
        }

        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        double seconds = (double) duration / 1000000000.0;
        System.out.println(String.format("\n\n\nElapsed time: %2.2fs", seconds));
    }


    private static String generateVfsUrl(String inputFile){
        //TODO: Make sure the inputFile isn't already a VFS URI.
        String ext = Files.getFileExtension(inputFile);
        String vfsUri = new File(inputFile).toURI().toString();
        logger.debug( "URI is '{}'", vfsUri );
        vfsUri = ext + ":" + vfsUri;
        logger.debug( "VFS URI is '{}'", vfsUri );
        return vfsUri;
    }

    private static boolean isVfsSupportedScheme(String inputFile) throws FileSystemException{
        String lower = Files.getFileExtension(inputFile).toLowerCase();
        for( String scheme: VFS.getManager().getSchemes()){
            if( lower.equals(scheme.toLowerCase())){
                return true;
            }
        }
        return false;
    }


    @SuppressWarnings("static-access")
    // Required for OptionBuilder below.
    private static ProfilerConfig processConfiguration(String[] args) throws IOException, URISyntaxException {
        ProfilerConfig config = new ProfilerConfig("/dataprof.properties");

        if (args.length == 0) {

            String pwd = Paths.get(".").toAbsolutePath().normalize().toString();
            logger.debug( String.format("pwd is %s", pwd   ) );
             //args = new String[]{ "-h" };

            // args = new String[] { "-sample_frequency", "1", "cities1000.txt" };
            // args = new String[] { "-sample_frequency", "1", "cities1000.zip" };
            // args = new String[] { "-sample_frequency", "1", "./cities1000.zip" };
            // args = new String[] { "-sample_frequency", "1", "allCountries.txt" };

            args = new String[]{ "-sample_frequency", "1", "src/test/resources/test1.csv" };
            //args = new String[]{ "-sample_frequency", "1", pwd + "/src/test/resources/test1.csv" };
            
            // args = new String[]{ "-sample_frequency", "1", "src/test/resources/test1.csv", "src/test/resources/test1.csv"};


            // This works, summarizes multiple files.
            // args = new String[]{ "-sample_frequency", "1", "src/test/resources/multiple_test.zip" };
            
            // The following with !test1.csv won't work because we don't properly handle files if they begin with Apache VFS schema 
            // FAILS: args = new String[]{ "-sample_frequency", "1", "src/test/resources/multiple_test.zip!test1.csv" };
            
            //args = new String[]{ "-sample_frequency", "1", "src/test/resources/multiple_test.tar" };
            //args = new String[]{ "-sample_frequency", "1", "src/test/resources/multiple_test.tgz" };
            // args = new String[]{ "-sample_frequency", "1", pwd + "/src/test/resources/multiple_test.tgz" };

            //args = new String[]{ "-sample_frequency", "1", "StormEvents_details-ftp_v1.0_d1950_c20170120.csv.gz" };

            // args = new String[]{ "-sample_frequency", "1", "src/test/resources/test_trailing_delim.csv" };
            
            // args = new String[]{ "-sample_frequency", "1", "-quote_character", "\"", "src/test/resources/propub/20170816_Documenting_Hate.csv" };
            
            //args = new String[]{ "-sample_frequency", "1", "-quote_character", "\"", "src/test/resources/propub/Civil_cases_1970_to_1987.txt" };
            //args = new String[]{ "-sample_frequency", "1", "-quote_character", "\"", "src/test/resources/propub/desegregation-orders/vol_data.csv" };
            //args = new String[]{ "-sample_frequency", "1", "-quote_character", "\"", "../sales_pipes.csv" };
            
            
            //args = new String[]{ "-sample_frequency", "1", "-quote_character", "\"", "src/test/resources/propub/federal-gun-cases.csv" };


            //TODO This fails because the file is already a VFS URL.
            //args = new String[]{ "-sample_frequency", "1", "zip:file:///C:/Users/Andy/projects/data_profiler/forpatad/src/test/resources/multiple_test.zip" };

            // Use a relative file
            // args = new String[]{ "-sample_frequency", "1", "src/test/resources/multiple_test.zip" };

            // This works, summarizes the single file within.
            // args = new String[]{ "-sample_frequency", "1", "src/test/resources/single_test.zip" };

            // This works, with or without trailing slash.
            //args = new String[]{ "-sample_frequency", "1", "src/test/resources" };
            //args = new String[]{ "-sample_frequency", "1", "src/test/resources/" };

            //TODO This does not work:
            // args = new String[]{ "-sample_frequency", "1", "src/test/resources/*.csv" };

             // Leading Apache Virtual Filesystem schema won't work.  It works only by file extension now.
             //args = new String[]{ "-sample_frequency", "1",
             //"-quote_character", "DQUOTE",
             //"tar://"  +  pwd + "/src/test/resources/multiple_test.tar"
             //};
        }

        Options options = initializeCommandLineOptions();

        // create the parser
        CommandLineParser parser = new BasicParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);

            if (line.hasOption("help")) {
                // automatically generate the help statement
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp("dataprof [options] [files...]", options);
                System.exit(0);
            }

            if (line.hasOption("quote_character")) {
                if ("DQUOTE".equals(line.getOptionValue("quote_character"))) {
                    config.quoteChar = '"';
                } else {
                    config.quoteChar = line.getOptionValue("quote_character").charAt(0);
                }
            }

            if (line.hasOption("separator_character")) {
                // TODO Need to finish implementing support for specifying
                // column separator character.
                char separator_char;
                if ("TAB".equals(line.getOptionValue("separator_character"))) {
                    separator_char = '\t';
                } else {
                    separator_char = line.getOptionValue("separator_character").charAt(0);
                }
            }

            if (line.hasOption("sample_frequency")) {
                config.sampleFrequency = Integer.parseInt(line.getOptionValue("sample_frequency"));
            }

            if (line.getArgs().length > 0) {
                for (String s : line.getArgs()) {
                    if( s.matches("^[\\w]{2}[\\w*]://.*") ) {  // check is string has at least two characters, followed by ://
                        System.err.println("Error: currently can handle if the filename includes a leading Apache VFS schema like xxx://");
                        System.exit(1);
                    } else {
                        File f = new File(s);
                        // TODO This does not work if the file begins with a scheme, such at tar://
                        String fcp = f.getCanonicalPath();
                       config.inputFiles.add(fcp);
                    }
                }
            } else {
                System.out.println("Nothing to process");
                System.exit(0);
            }
        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }
        return config;
    }

    private static Options initializeCommandLineOptions() {
        Options options = new Options();

        Option option;

        option = OptionBuilder.withArgName("character").hasArg()
                .withDescription("use given character as quote character").create("quote_character");

        options.addOption(option);

        option = OptionBuilder.withArgName("character").hasArg()
                .withDescription("use given character as column separator character NOT FULLY IMPLEMENTED")
                .create("separator_character");

        options.addOption(option);

        option = OptionBuilder.withArgName("number").hasArg().withDescription("use given number as sample frequency")
                .create("sample_frequency");

        options.addOption(option);

        options.addOption("h", "help", false, "display help");
        return options;
    }
}
