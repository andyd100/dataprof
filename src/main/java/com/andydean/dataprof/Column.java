package com.andydean.dataprof;

import java.util.ArrayList;
import java.util.List;

public class Column {

    private String fileSource;
    private String header;
    private List<String> values;

    public Column() {
        this.values = new ArrayList<String>();
    }

    public Column(String header, String file) {
        this();
        this.setHeader(header);
        this.setFileSource(file);
    }

    public Column(List<String> vals) {
        this();
        for (String s : vals) {
            this.add(s);
        }
    }

    public boolean add(String s) {
        this.values.add(s);
        return true; // To make it behave like List.add()
    }

    public List<String> getValues() {
        return values;
    }

    public void setHeader(String s) {
        this.header = s;
    }

    public String getHeader() {
        return this.header;
    }

    public void setFileSource(String s) {
        this.fileSource = s;
    }

    public String getFileSource() {
        return this.fileSource;
    }
}
