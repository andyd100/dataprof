package com.andydean.dataprof;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import com.andydean.dataprof.Column;
import com.andydean.dataprof.ColumnSummary;
import com.andydean.dataprof.FileType;
import com.andydean.dataprof.ProfileEngine;
import com.andydean.dataprof.ProfilerConfig;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple ProfileEngine.
 */
public class PropubFileTests extends TestCase {

    // TODO Should rename this class.

    /**
     * Create the test case
     *
     * @param testName
     *            name of the test case
     */
    public PropubFileTests(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(PropubFileTests.class);
    }


    public void testFileSeparator() throws IOException {
        Reader reader = new FileReader("src/test/resources/propub/20170816_Documenting_Hate.csv");
        char separator = ProfileEngine.determineFileSeparator(reader, 1000);
        assertEquals(',', separator);
    }

    public void testReadingFromFile() throws IOException, URISyntaxException {

        File f = new File("src/test/resources/propub/20170816_Documenting_Hate.csv");
        String s = f.getCanonicalPath();

        // List<Column> columns = ProfileEngine.sampleFileIntoColumns( s, ',', '\0', 100, 1);
        List<Column> columns = ProfileEngine.sampleFileIntoColumns( s, ',', '"', 100, 1);
        ProfileEngine pe = new ProfileEngine();
        ProfilerConfig config = new ProfilerConfig("/dataprof.properties");

        for (Column column : columns) {
            System.out.println("\n\n ------------------------------\n");
            ColumnSummary cs = pe.analyzeColumn(config, column);
            cs.printSummary();
        }

        ColumnSummary cs;
        cs = pe.analyzeColumn(config, columns.get(0));
        assertEquals("Article Date", cs.header);
        //TODO Need to do something here to assert that the column is being recognized as a Date type.

        cs = pe.analyzeColumn(config, columns.get(1));
        assertEquals("Article Title", cs.header);

      

    }


}
