package com.andydean.dataprof;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import com.andydean.dataprof.Column;
import com.andydean.dataprof.ColumnSummary;
import com.andydean.dataprof.FileType;
import com.andydean.dataprof.ProfileEngine;
import com.andydean.dataprof.ProfilerConfig;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple ProfileEngine.
 */
public class AppTest extends TestCase {

    // TODO Should rename this class.

    /**
     * Create the test case
     *
     * @param testName
     *            name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true);
    }

    public void testFileTypeDetection() {
        assertEquals(FileType.CSV_FILE, ProfileEngine.determineFileType("foo.csv"));
    }

    public void testFileSeparator() throws IOException {
        Reader reader = new FileReader("src/test/resources/test1.csv");
        char separator = ProfileEngine.determineFileSeparator(reader, 1000);
        assertEquals(',', separator);
    }

    public void testReadingFromFile() throws IOException, URISyntaxException {

        File f = new File("src/test/resources/test1.csv");
        String s = f.getCanonicalPath();

        List<Column> columns = ProfileEngine.sampleFileIntoColumns( s, ',', '\0', 100, 1);
        ProfileEngine pe = new ProfileEngine();
        ProfilerConfig config = new ProfilerConfig("/dataprof.properties");

        for (Column column : columns) {
            System.out.println("\n\n ------------------------------\n");
            ColumnSummary cs = pe.analyzeColumn(config, column);
            cs.printSummary();
        }

        ColumnSummary cs;
        cs = pe.analyzeColumn(config, columns.get(0));
        assertEquals((Integer) 3, cs.typesMap.get("pattern.number.signed_integer.regex"));

        cs = pe.analyzeColumn(config, columns.get(1));
        assertEquals((Integer) 3, cs.typesMap.get("pattern.number.signed_integer.regex"));

        cs = pe.analyzeColumn(config, columns.get(13));
        assertEquals( cs.getHeader(), "FixedWidth3");
        assertEquals( cs.getPrecision(), "fixed-width(3)");

    }

    public void testAllIntegerColumn() throws IOException, URISyntaxException {
        // List<String> column = Arrays.asList("1234", "-1234", "foo", "bar",
        // "baz", "foobar");

        Column column = new Column(Arrays.asList("1234", "-1234", "foo", "bar", "baz", "foobar"));
        ProfileEngine pe = new ProfileEngine();
        ProfilerConfig config = new ProfilerConfig("/dataprof.properties");
        ColumnSummary cs = pe.analyzeColumn(config, column);

        cs.printSummary();

        assertEquals("check longest entry in column", 6, cs.maxLength);
        assertEquals("make sure first long string is what gets reported", "foobar", cs.longestCell);

        assertEquals("check shortest entry in column", 3, cs.minLength);
        assertEquals("make sure first short string is what gets reported", "foo", cs.shortestCell);

        assertEquals((Integer) 2, (Integer) cs.typesMap.get("pattern.number.signed_integer.regex"));

    }
}
